<?php
declare (strict_types=1);

namespace base_service;

use http\Exception\InvalidArgumentException;
use think\Exception;
use think\exception\ClassNotFoundException;
use think\exception\ValidateException;
use think\facade\Cache;

abstract class BaseService
{
    protected $limit = 10;
    protected $page = 1;
    protected $orderBy;
    protected $modelName;
    protected $fields;
    protected $pros;

    /**
     * redis  hash 名
     * @var
     */
    protected $cacheHash;

    /**
     * 缓存 键名前缀
     * @var
     */
    protected $cachePre;

    /**
     * xxModel::class
     * @return mixed
     */
    abstract function setModelName();

    public function __construct()
    {
        $this->setModelName();
        $reflect = new \ReflectionClass($this->modelName);
        $this->pros = $reflect->getDefaultProperties();
        $this->orderBy = '-' . $this->pros['pk'];

    }

    /**
     * @param $conditions
     * @return mixed
     */
    protected function builderQuery($conditions, $join = null)
    {
        $schema = $this->pros['schema'];
        $fields = array_keys($schema);
        $regSuffixHash = $this->pros['regSuffixHash'];
        $query = $this->modelName::newQuery();
        if ($join) {
            $query->with($join);
        }
        foreach ($conditions as $k => $v) {
            if (is_string($v)) $v = trim($v);
            if (empty($v) && ((is_numeric($v) && $v != 0) || !is_numeric($v))) {
                continue;
            }
            if (in_array($k, $fields)) {
                $query->where($k, $regSuffixHash['eq'], $v);
            } else {
                foreach ($regSuffixHash as $suf => $rep) {
                    $reg = "/\_{$suf}$/";
                    $field = preg_replace($reg, '', $k);
                    if (preg_match($reg, $k) && in_array($field, $fields)) {
                        if ($suf === 'like') {
                            $v .= '%';
                        }
                        $query->where($field, $rep, $v);
                    }
                }
            }
        }
        return $query;
    }

    /**
     * 查询的时候根据model设置的filterFields过滤字段
     * @param $query
     * Date: 2021-4-26
     * Time: 0:13\
     */
    protected function filterFields($query, $conditions)
    {
        $filterFields = $this->pros['filterFields'] ?? [];
        $fields = array_keys($this->pros['schema']);
        if (isset($conditions['s_field'])) {
            $fields = array_intersect($fields, explode(',', $conditions['s_field']));
        }
        if (!empty($filterFields)) {
            $fields = array_diff($fields, $filterFields);
        }
        if (empty($fields)) {
            throw new Exception('Invalid s_field', 400);
        }
        $query->field(implode(',', $fields));
    }

    /**
     * @param $conditions
     * @return mixed
     */
    public function isExist($conditions)
    {
        try {
            $result = $this->builderQuery($conditions)->find();
            return response(true, 200, 'success', !empty($result));
        } catch (\Exception $e) {
            return response(false, 500, $e->getMessage());
        }
    }

    /**
     * @param $conditions
     * @param null $join
     * @param false $filter
     * @return array|\think\Response
     */
    public function queryPage($conditions, $join = null)
    {
        try {
            $total = $this->count($conditions);
            $limit = (int)($conditions['limit'] ?? $this->limit);
            $page = (int)($conditions['page'] ?? $this->page);
            $query = $this->builderQuery($conditions, $join)->page($page, $limit);
            $this->orderBy($conditions, $query);
            $this->filterFields($query, $conditions);
            $list = $this->select($query)->toArray();
            $pagination = $this->getPagination($page, $total, $limit);
            return response(true, 200, 'success', ['list' => $list, 'pagination' => $pagination]);
        } catch (\Exception $e) {
            return response(false, 500, $e->getMessage());
        }
    }

    /**
     * 过滤字段
     * @param $params
     * @param null $join
     * @return array|\think\Response
     */
    public function filterQueryPage($params, $join = null)
    {
        try {
            $allFields = array_keys($this->pros['schema']);
            $allowFields = $this->pros['allowFields'];
            $sFields = isset($params['s_field']) ? explode(',', $params['s_field']) : $allFields;
            if (!empty($allowFields)) {
                $sFields = array_intersect($allowFields, $sFields);
            }
            $params['s_field'] = implode(',', $sFields);
            return $this->queryPage($params, $join);
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e->getMessage());
        }
    }

    /**
     * @param $conditions
     * @param $limit
     * @param $page
     * @return mixed
     */
    public function select($query, $join = null)
    {
        /*if ($join){
            $list = $query->withJoin($join)->select();
            foreach ($list as $k => $item){
                $item->{$join};
            }
        }else{
            $list = $query->select();
        }*/
        return $query->select();
    }

    public function query($conditions, $join = null)
    {
        try {
            $total = $this->count($conditions);
            $limit = 100;
            $page = 1;
            $pagination = $this->getPagination($page, $total, $limit);
            $query = $this->builderQuery($conditions, $join)->limit($limit)->page($page, $limit);
            $this->orderBy($conditions, $query);
            $this->filterFields($query, $conditions);
            $list = [];
            do {
                $list = array_merge($list, $this->select($query)->toArray());
                $page++;
                $query->page($page, $limit);
            } while ($page <= $pagination['pages']);
            return response(true, 200, 'success', ['list' => $list]);
        } catch (\Exception $e) {
            //$sql = $this->modelName::getLastSql();
            return response(false, 500, $e->getMessage());
        }
    }

    /**
     * @param $conditions
     * @return int
     */
    public function count($conditions): int
    {
        return $this->builderQuery($conditions)->count();
    }

    /**
     * @param $conditions
     * @param $query
     * @return mixed
     */
    protected function orderBy($conditions, $query)
    {
        $orderBy = isset($conditions['orderBy']) && !empty(trim($conditions['orderBy'])) ? trim($conditions['orderBy']) : $this->orderBy;
        $fields = explode(",", $orderBy);
        $reg = "/^-/";
        $orderArr = [];
        foreach ($fields as $field) {
            $field = trim($field);
            $sort = preg_match($reg, $field) ? 'desc' : 'asc';
            $field = trim(preg_replace($reg, '', $field));
            if (in_array($field, array_keys($this->pros['schema']))) {
                $orderArr[$field] = $sort;
            } else {
                $orderArr = [];
                break;
            }
        }
        if (!empty($orderArr)) {
            $query->order($orderArr);
        }
        return $query;
    }

    /**
     * @param $page
     * @param $total
     * @param $limit
     * @return array
     */
    protected function getPagination($page, $total, $limit): array
    {
        $pages = $this->getPages($total, $limit);
        return [
            'page' => $page,
            'pages' => $pages,
            'limit' => $limit,
            'total' => $total
        ];
    }

    /**
     * @param $total
     * @param $limit
     * @return int
     */
    protected function getPages($total, $limit): int
    {
        return $limit > 0 && $total > 0 ? (int)ceil($total / $limit) : 1;
    }

    /**
     * 根据id获取数据
     * @param $id
     * @param null $join
     * @param null $createBy
     * @return array|\think\Response
     */
    public function read($id, $join = null,$createBy = null)
    {
        try {
            if (empty($id)) {
                throw new Exception('Invalid id');
            }
            $row = $this->modelName::with($join)->find($id);
            if (!$row || (!empty($createBy) && $row->create_by != $createBy)) {
                throw new Exception('source not found', 404);
            }
            return response(true, 200, 'success', $row);
        } catch (\Exception $e) {
            return response(false, $e->getCode(), $e->getMessage());
        }
    }

    /**
     * 删除
     * @param $id
     * @param bool $isSoftDelete 软删除，默认为true
     * @return array
     */
    public function delete($id, $isSoftDelete = true)
    {
        try {
            $row = $this->modelName::find($id);
            if (empty($row)) {
                throw new Exception('资源不存在', 404);
            }
            if ($isSoftDelete) {
                if ($row->delete_time > 0) {
                    throw new Exception('请不要重复删除');
                }
                $row->save(['delete_time' => time()]);
            } else {
                $row->force()->delete();
            }
            return successResponse($row);
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 默认的创建方法，支持验证数据
     * 优先采用$validate自定义
     * @param $params
     * @param null $validateScene 验证场景,必须定义model及validate
     * @param null $validate 自定义验证方法
     * @param $createBy
     * @return array
     */
    public function createDefault($params, $validateScene = null, $validate = null, $createBy = '')
    {
        try {
            foreach ($params as $k => $v) {
                $params[$k] = is_string($v) ? trim($v) : $v;
            }
            //检查
            $this->validate($params, $validateScene, $validate);
            $params['create_by'] = $params['update_by'] = $this->getCreateBy($createBy);
            $row = $this->modelName::create($params);
            return successResponse($row->toArray());
        } catch (ClassNotFoundException | InvalidArgumentException $e) {
            return errorResponse(500, $e->getMessage());
        } catch (ValidateException | \Exception $e) {
            return errorResponse(400, $e->getMessage());
        }
    }

    /**
     * 获取创建人
     * 优先获取token的用户的 user_name
     * 其次 user_id
     * 都没有则获取 ip
     * @param $createBy
     * @return string
     */
    protected function getCreateBy($createBy = '')
    {
        if (empty($createBy)) {
            if (isset(request()->userInfo)) {
                $useInfo = request()->userInfo;
                switch (true) {
                    case isset($useInfo->user_name) :
                        $createBy = $useInfo->user_name;
                        break;
                    case isset($useInfo->user_id) :
                        $createBy = $useInfo->user_id;
                        break;
                    default :
                        $createBy = get_ip();
                }
            }
        }
        return $createBy;
    }

    /**
     * @param $id
     * @param $params
     * @param null $validateScene
     * @param null $validate
     * @return array
     */
    public function updateDefault($id, $params, $validateScene = null, $validate = null, $createBy = '')
    {
        try {
            $row = $this->modelName::find($id);
            if (empty($row)) {
                throw new Exception('source not exist', 404);
            }
            foreach ($params as $k => $v) {
                $params[$k] = is_string($v) ? trim($v) : $v;
            }
            $this->validate($params, $validateScene, $validate);
            $params['update_by'] = $this->getCreateBy($createBy);
            $row->save($params);
            return successResponse($row->toArray());
        } catch (ClassNotFoundException | InvalidArgumentException $e) {
            return errorResponse(500, $e->getMessage());
        } catch (ValidateException | \Exception $e) {
            return errorResponse(400, $e->getMessage());
        }
    }

    /**
     * 优先采用$validate自定义验证方法
     * @param $params
     * @param null $validateScene 验证场景，必须先定义model关联validate。并添加场景
     * @param null $validate 自定义验证方法
     * @throws Exception
     */
    protected function validate($params, $validateScene = null, $validate = null)
    {
        //检查
        if ($validate) {
            if (!$validate instanceof \think\Validate) throw new InvalidArgumentException('unexpected instanceof validate');
            $validate->check($params);
        } else if ($validateScene && $this->pros['validate']) {
            if (!class_exists($this->pros['validate'])) throw new ClassNotFoundException('class for validate is undefined');
            $validate = validate($this->pros['validate']);
            if (!$validate instanceof \think\Validate) throw new InvalidArgumentException('unexpected instanceof validate');
            if (!$validate->hasScene($validateScene)) throw new Exception('scene not found');
            $validate->scene($validateScene)->check($params);
        }
    }

    public function recover($id)
    {
        try {
            $row = $this->modelName::find($id);
            if ($row->delete_time == 0) {
                throw new Exception('请不要重复恢复删除');
            }
            $row->save(['delete_time' => 0]);
            return response(true, 200, 'success', $row);
        } catch (\Exception $e) {
            return response(false, 400, $e->getMessage());
        }
    }

    public static function getJoinFuc($params)
    {
        $fuc = isset($params['join_fuc']) && is_string($params['join_fuc']) && trim($params['join_fuc']) ? explode(',', $params['join_fuc']) : [];
        if (!empty($fuc)) {
            $fuc = array_map(function ($item) {
                return is_string($item) ? trim($item) : $item;
            }, $fuc);
        }
        return $fuc;
    }

    /**
     * redis缓存hash名
     * @return string
     */
    protected function getCacheHashKey()
    {
        return empty($this->cacheHash) ? strtolower("{$this->pros['name']}_hash}") : $this->cacheHash;
    }

    /**
     * k-v 缓存 key
     * @param $id
     * @return string
     */
    protected function getCachePre($id)
    {
        return (empty($this->cachePre) ? strtolower("id_") : $this->cachePre) . $id;
    }

    /**
     * 根据id获取redis hash-key对应的值
     * @param $id
     * @param null $cacheHashKey
     * @param null $cachePre
     * @return array
     * @throws Exception
     */
    public function getCacheHash($id, $cacheHashKey = null, $cachePre = null): array
    {
        $cache = Cache::store('redis')->handler();
        if (!$cacheHashKey) {
            $cacheHashKey = $this->getCacheHashKey();
        }
        if (!$cachePre) {
            $cachePre = $this->getCachePre($id);
        }
        if ($cache->hExists($cacheHashKey, $cachePre)) {
            $row = $cache->hGet($cacheHashKey, $cachePre);
        } else {
            $row = $this->cacheHash($id, $cacheHashKey, $cachePre);
        }
        return json_decode($row, true);
    }

    /**
     * 判断是否存在redis hash-key
     * @param $id
     * @param null $cacheHashKey
     * @param null $cachePre
     * @return bool
     */
    public function isCacheHash($id, $cacheHashKey = null, $cachePre = null): bool
    {
        $cache = Cache::store('redis')->handler();
        if (!$cacheHashKey) {
            $cacheHashKey = $this->getCacheHashKey();
        }
        if (!$cachePre) {
            $cachePre = $this->getCachePre($id);
        }
        return $cache->hExists($cacheHashKey, $cachePre);
    }

    /**
     * 生成redis hash缓存
     * @param $id
     * @param null $cacheHashKey
     * @param null $cachePre
     * @param null $data
     * @return string
     * @throws Exception
     */
    public function cacheHash($id, $cacheHashKey = null, $cachePre = null, $data = null): string
    {
        $cache = Cache::store('redis')->handler();
        if (!$cacheHashKey) {
            $cacheHashKey = $this->getCacheHashKey();
        }
        if (!$cachePre) {
            $cachePre = $this->getCachePre($id);
        }
        if (!$data) {
            $row = $this->modelName::find($id);
            if (empty($row)) {
                throw new Exception('source not exist', 404);
            }
            $data = $row->toJson();
        } else {
            $data = json_encode($data);
        }
        $cache->hSet($cacheHashKey, $cachePre, $data);
        return $data;
    }

    /**
     * 清除hash-key
     * @param $id
     * @param null $cacheHashKey
     * @param null $cachePre
     */
    public function clearHashKey($id, $cacheHashKey = null, $cachePre = null)
    {
        $cache = Cache::store('redis')->handler();
        if (!$cacheHashKey) {
            $cacheHashKey = $this->getCacheHashKey();
        }
        if (!$cachePre) {
            $cachePre = $this->getCachePre($id);
        }
        if ($cache->hExists($cacheHashKey, $cachePre)) {
            $cache->hDel($cacheHashKey, $cachePre);
        }
    }

    /**
     * 获取k-v缓存
     * @param $id
     * @param null $cachePre
     * @return mixed
     * @throws Exception
     */
    public function getCache($id, $cachePre = null)
    {
        if (!$cachePre) {
            $cachePre = $this->getCachePre($id);
        }
        if (Cache::has($cachePre)) {
            $row = Cache::get($cachePre);
        } else {
            $row = $this->cache($id, $cachePre);
        }
        return json_decode($row, true);
    }

    /**
     * 生成k-v缓存
     * @param $id
     * @param null $cachePre
     * @param null $data
     * @param null $expire
     * @return false|string
     * @throws Exception
     */
    public function cache($id, $cachePre = null, $data = null, $expire = null)
    {
        if (!$cachePre) {
            $cachePre = $this->getCachePre($id);
        }
        if (!$data) {
            $row = $this->modelName::find($id);
            if (empty($row)) {
                throw new Exception('source not exist', 404);
            }
            $data = $row->toJson();
        } else {
            $data = json_encode($data);
        }
        Cache::set($cachePre, $data, $expire);
        return $data;
    }

    /**
     * 生成k-v缓存
     * @param $id
     * @param null $cachePre
     */
    public function clearCache($id, $cachePre = null)
    {
        if (!$cachePre) {
            $cachePre = $this->getCachePre($id);
        }
        if (Cache::has($cachePre)) {
            Cache::delete($cachePre);
        }
    }
}
