<?php
declare (strict_types=1);

namespace base_service;


use think\Model;

abstract class BaseModel extends Model
{

    protected $regSuffixHash = [
        'eq' => '=',
        'gt' => '>',
        'gte' => '>=',
        'lt' => '<',
        'lte' => '<=',
        'like' => 'like',
        'ne' => '<>'
    ];
    protected $filterFields = [];

    protected $allowFields = [];

    protected $validate = null;
}